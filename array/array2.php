<?php
// array_count_values

$array = array(1, "hello", 1, "world", "hello");
echo"<pre>ARRAY_COUNT_VALUES<br>";
print_r(array_count_values($array));

//array_diff
echo"<br>ARRAY_DIFF<br>";
$array1 = array("a" => "green", "red", "blue", "red"); // different values of array1 will only be printed
$array2 = array("b" => "black", "yellow", "red");
$result = array_diff($array1, $array2);echo'<br>';
print_r($result);

//array_intersect
echo"<br>ARRAY_INTERSECT";
$result1 =array_intersect($array1,$array2);  //give array1 values which are present on other arrays 
echo'<br>';
print_r($result1);

//array_map
function square($n)
{
    return($n * $n);
}
echo"<br> ARRAY_MAP<br>";
$a = array(1, 2, 3, 4, 5);
$b = array_map("square", $a);
echo'<br>';
print_r($b);

//array_merge
echo"<br> ARRAY_MERGE";
$array3 = array("a"=>"black",2=>"blue");
$result2 = array_merge($array1,$array3);
echo'<br>';
print_r($result2);

// array_merge_recursive
echo"<br> ARRAY_MERGE_RECURSIVE";
$ar1 = array("color" => array("favorite" => "red"), 5);
$ar2 = array(10, "color" => array("favorite" => "green", "blue"));
$result3 = array_merge_recursive($ar1, $ar2);

echo'<br>';print_r($result3);

//array_push
array_push($array1,"white","grey");
echo"<br>ARRAY_PUSH";echo'<br>';
print_r($array1);


//array_reduce
function sum($carry, $item)
{
    $carry += $item;
    return $carry;
}
echo"<br> ARRAY_REDUCE";
function product($carry, $item)
{
    $carry *= $item;
    return $carry;
}

$a = array(1, 2, 3, 4, 5);
$x = array();
echo'<br>';
var_dump(array_reduce($a, "sum")); // int(15)
var_dump(array_reduce($a, "product", 10)); // int(1200), because: 10*1*2*3*4*5
var_dump(array_reduce($x, "sum", "No data to reduce")); // string(17) "No data to reduce"

//searching in the array
$array = array(0 => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');
echo"<br> ARRAY_SEARCH";
$key = array_search('green', $array);echo "$key"; // $key = 2;
$key = array_search('red', $array);   // $key = 1;

//shifting in the array
echo"<br>ARRAY_SHIFT";
$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_shift($stack);
echo'<br>';
print_r($stack);


//unshift in array
echo"<br> ARRAY_UNSHIFT<br>";
$queue = array("orange", "banana");
array_unshift($queue, "apple", "raspberry");
echo'<br>';
print_r($queue);


//extract in an array
echo"<br> ARRAY_EXTRACT";
$size = "large";
$var_array = array("color" => "blue",
                   "size"  => "medium",
                   "shape" => "sphere");
extract($var_array, EXTR_PREFIX_SAME, "wddx");

echo "$color, $size, $shape, $wddx_size\n";
?>
