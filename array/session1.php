<?php
// Start the session
session_start();
?>
<html>
<body>

<?php
// Set session variables
$_SESSION["favcolor"] = "green";
$_SESSION["favanimal"] = "cat";
echo "Session variables are set.";
echo'</br>';
echo" get php session variable values </br>";
print_r($_SESSION);

// unset the session
session_unset();

echo'</br></br>';
print_r($_SESSION);

//destroy the session
session_destroy();

?>

</body>
</html>
